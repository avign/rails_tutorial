# README


This is a simpleBlog Application with Basic Crud Functionalities for users to login and create microposts. Also user can follow other users to see their microposts, comment and Like Them 

This App has been built using Ruby On Rails.


You can find The Project running live here https://cryptic-thicket-86286.herokuapp.com/

default Username - example@railstutorial.org <br>
default password - foobar


![ScreenShot](app/assets/images/Screenshot from 2021-03-29 10-29-36.png)
![ScreenShot](app/assets/images/Screenshot from 2021-03-29 10-30-02.png)
![ScreenShot](app/assets/images/Screenshot from 2021-03-29 10-30-21.png)
